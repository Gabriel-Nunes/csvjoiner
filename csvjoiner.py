# -*- coding: utf-8 -*-

__author__ = "GABRIEL LIMA NUNES"

from subprocess import Popen
from tkinter import Tk, messagebox, filedialog
import csv
import os


def question(titulo_janela, pergunta):
    root = Tk()
    root.withdraw()
    root.answer = messagebox.askyesno(titulo_janela, pergunta)
    return root.answer


def select_files():
    root = Tk()
    root.withdraw()
    root.filenames = filedialog.askopenfilenames(initialdir="/", title="Selecione os arquivos CSV...",
                                                filetypes=(("csv files", "*.csv"), ("all files", "*.*")))
    return list(root.filenames)


# Ask about headers
possuiCabecalho = question("csvjoiner", "Seus dados possuem cabeçalho?")

# Select files
originalFiles = select_files()
csvData = []

# Salva o cabeçalho na planilha
if possuiCabecalho:
    openedFile = open(originalFiles[0])
    fileReader = csv.reader(openedFile)
    fileData = list(fileReader)
    csvData.append(fileData[0])
    openedFile.close()
    del openedFile, fileData, fileReader

    # Abre cada arquivo
    for file in originalFiles:
        csvFile = open(file)
        fileReader = csv.reader(csvFile)
        fileData = list(fileReader)
        csvFile.close()

        # Salva o conteúdo do arquivo na lista csvData, exceto o cabeçalho, ignorando linhas vazias
        for i in range(1, len(fileData[1:])):
            if len(fileData[i]) > 0:
                csvData.append(fileData[i])

    # Salva conteúdo da planilha em um arquivo 'Geral'
    newFile = open(os.path.dirname(originalFiles[0]) + r'\GERAL.csv', mode='w', newline='')
    writer = csv.writer(newFile)
    writer.writerows(csvData)
    newFile.close()

    Popen(r'explorer /select,' + os.path.abspath(originalFiles[0]))

else:
    # Abre cada arquivo
    for file in originalFiles:
        csvFile = open(file)
        fileReader = csv.reader(csvFile)
        fileData = list(fileReader)
        csvFile.close()

        # Salva o conteúdo do arquivo na lista csvData, exceto o cabeçalho, ignorando linhas vazias
        for i in range(len(fileData)):
            if len(fileData[i]) > 0:
                csvData.append(fileData[i])

    # Salva conteúdo da planilha em um arquivo 'Geral'
    newFile = open(os.path.dirname(originalFiles[0]) + r'\GERAL.csv', mode='w', newline='')
    writer = csv.writer(newFile)
    writer.writerows(csvData)
    newFile.close()

    Popen(r'explorer /select,' + os.path.abspath(originalFiles[0]))
