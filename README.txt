# CSVjoiner

This program join the content of different csv files in a single one.

Usage:

- Run 'CSV Joiner.exe' on 'dist' folder
- Choose the files to join
- Check "Geral.csv" file at the same folder of original files
